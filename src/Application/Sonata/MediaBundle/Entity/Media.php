<?php

declare(strict_types=1);

namespace App\Application\Sonata\MediaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Sonata\MediaBundle\Entity\BaseMedia;
use App\Application\Sonata\ClassificationBundle\Entity\SonataClassificationCategory as SonataClassificationCategory;

/**
 * @ORM\Entity
 * @ORM\Table(name="media__media")
 */
class Media extends BaseMedia
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @var SonataClassificationCategory|null
     */
    protected ?object $category = null;

    public function getId(): ?int
    {
        return $this->id;
    }


    public function getCategory(): ?object
    {
        return $this->category;
    }

    public function setCategory(?object $category = null): void
    {
        $this->category = $category;
    }
}